/*
*@author      : Aditya Gaykar
*@version     : 2.0
*@date 	      : 11 November 2012
*@discription : Simple code to implement In-order, Pre-order, Post-order traversal of a tree.
* Now it also computes Inorder, Preorder and Postorder successor results for a tree node.
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

typedef struct node{
	int key;
	struct node *parent;
	struct node *left;
	struct node *right;
}NODE;

NODE* root;

NODE* create_node(){
	return malloc(sizeof(NODE));
}

void insert_node(NODE *p,int k){
	NODE *tmp = create_node();
	tmp->key = k;
	if( k > p->key ) {
		if( p->right != NULL )
			insert_node(p->right,k);
		else {
		//	p->right->parent = p;
			p->right = tmp;
			tmp->parent = p;
			printf("Node %d inserted successfully!\n",k);
		}
	} else {
		if( p->left != NULL )
			insert_node(p->left,k);
		else {
		//	p->left->parent = p;
			p->left = tmp;
			tmp->parent = p;
			printf("Node %d inserted successfully!\n",k);
		}
	}
}

void inOrder(NODE* p) {
	if(p->left != NULL)
		inOrder(p->left);
	printf(" %d",p->key);
	if(p->right != NULL)
		inOrder(p->right);
}

void preOrder(NODE* p) {
	printf(" %d",p->key);
	if(p->left != NULL)
		preOrder(p->left);
	if(p->right != NULL)
		preOrder(p->right);
}

void postOrder(NODE* p) {
	if(p->left != NULL)
		postOrder(p->left);
	if(p->right != NULL)
		postOrder(p->right);
	printf(" %d",p->key);
}

NODE* find_node(NODE* r,int k) {
	NODE* tmp = r;
	if(r == NULL)
		return NULL;
	if(tmp->key == k) {
		printf("Key %d found!\n",tmp->key);
		return tmp;
	}
	else if(k <= tmp->key )
		return find_node(tmp->left,k);
	else
		return find_node(tmp->right,k);
}

int min_node(NODE* n) {
	while (n->left != NULL)
		n = n->left;
	return (int)n->key;
}

int inOrderSucc(int k) {
	NODE* p = (NODE*)find_node(root,k);
	if( p != NULL ){
		if(p->right != NULL)
			return (int)min_node(p->right);
		NODE *tmp = create_node();
		if(p->parent != NULL) {
			tmp = p->parent;
			while(tmp!=NULL && p == tmp->right) {
				p = tmp;
				tmp = p->parent;
			}
			return tmp->key;
		} else {
			printf("Successor does not exists!\n");
			return -32767;
		}
	} else {
		printf("Key does not exists!\n");
		return -32767;
	}
}

int preOrderSucc(int k) {
	NODE* p = (NODE*)find_node(root,k);
	if( p != NULL ){
		if(p->left != NULL)
			return (int)p->left->key;
		else if ( p->right != NULL )
			return (int)p->right->key;
		else {
			NODE *tmp = create_node();
			if(p->parent != NULL) {
				tmp = p->parent;
				while(tmp!=NULL && tmp->right == NULL ) {
					p = tmp;
					tmp = p->parent;
				}
				return (int)tmp->right->key;
			} else {
				printf("Successor does not exists!\n");
				return -32767;
			}
		}
	} else {
		printf("Key does not exists!\n");
		return -32767;
	}
}

int isLeaf(NODE *p) {
	if( p->left == NULL && p->right == NULL)
		return 1;
	else
		return 0;
}

int postOrderSucc(int k) {
	NODE* p = (NODE*)find_node(root,k);
	if( p != NULL ){
		if(isLeaf(p) && p->key > p->parent->key && p != root)
			return p->parent->key;
		else if(isLeaf(p) && p->key <= p->parent->key && p != root)
			return p->parent->key;
		else if( !isLeaf(p) && p->parent != root && p != root ) {
			if(p->parent->right == NULL)
				return p->parent->key;
			else 
				return p->parent->right->key;
		}
		else {
			NODE *tmp = create_node();
			if(p->parent == root) {
				tmp = root->right;
				while( tmp->left != NULL ) {
					p = tmp->left;
					tmp = tmp->left;
				}
				return p->key;
			} else {
				printf("Successor does not exists!\n");
				return -32767;
			}
		}
	} else {
		printf("Key does not exists!\n");
		return -32767;
	}
}


int main(int argc, char* argv[]){
	int i;
	NODE *n;
	root = create_node();
	/*
	root->key = 1;
	for(i=2;i<=10;i++){
		insert_node(root,i);
	}
	*/
	printf("Enter root node : ");
	scanf("%d",&i);
	root->key = i;
	while(1){
		int tmp;
		printf("Enter next element : ");
		scanf("%d",&tmp);
		insert_node(root,tmp);
		printf("Exit (0/1) ? : ");
		scanf("%d",&tmp);
		if( tmp == 1 )
			break;
	}
	printf("In-order traversal is as follows ... \n");
	inOrder(root);
	printf("\n\n");
	printf("Pre-order traversal is as follows ... \n");
	preOrder(root);
	printf("\n\n");
	printf("Post-order traversal is as follows ... \n");
	postOrder(root);
	while(1){
		printf("\n\nEnter the node key : ");
		int KEY;
		scanf("%d",&KEY);
		int inVal = (int)inOrderSucc(KEY);
		if( inVal != -32767)
			printf("Required Inorder successor : %d\n",inVal);
		int preVal = (int)preOrderSucc(KEY);
		if( preVal != -32767)
		printf("Required Preorder successor : %d\n",preVal);
		int postVal = (int)postOrderSucc(KEY);
		if( postVal != -32767)
			printf("Required Postorder successor : %d\n",postVal);
		int tmp = 0;
		printf("Exit (0/1) ? : ");
		scanf("%d",&tmp);
		if(tmp == 1)
			break;
	}
	return 0;
}

/*
OUTPUT:

Aditya-Gaykars-MacBook-Pro:graph adityagaykar$ gcc -o tree tree.c
Aditya-Gaykars-MacBook-Pro:graph adityagaykar$ ./tree
Enter root node : 50
Enter next element : 40
Node 40 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 30
Node 30 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 20
Node 20 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 10
Node 10 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 60
Node 60 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 55 
Node 55 inserted successfully!
Exit (0/1) ? : 0
Enter next element : 70
Node 70 inserted successfully!
Exit (0/1) ? : 1
In-order traversal is as follows ... 
 10 20 30 40 50 55 60 70

Pre-order traversal is as follows ... 
 50 40 30 20 10 60 55 70

Post-order traversal is as follows ... 
 10 20 30 40 55 70 60 50

Enter the node key : 10
Key 10 found!
Required Inorder successor : 20
Key 10 found!
Required Preorder successor : 60
Key 10 found!
Required Postorder successor : 20
Exit (0/1) ? : 0


Enter the node key : 50
Key 50 found!
Required Inorder successor : 55
Key 50 found!
Required Preorder successor : 40
Key 50 found!
Successor does not exists!
Exit (0/1) ? : 0


Enter the node key : 30
Key 30 found!
Required Inorder successor : 40
Key 30 found!
Required Preorder successor : 20
Key 30 found!
Required Postorder successor : 40
Exit (0/1) ? : 0


Enter the node key : 10
Key 10 found!
Required Inorder successor : 20
Key 10 found!
Required Preorder successor : 60
Key 10 found!
Required Postorder successor : 20
Exit (0/1) ? : 1


*/
