#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>

int main( int argc, char** argv){
	
	int numprocs, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if(rank == 0){
		long global_sum = 0,sum = 0;	
		printf(" >>> MASTER started :  [ OK ]\n");
		MPI_Recv(&sum, 1, MPI_LONG, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		global_sum+=sum;
		printf(" >>> Slave1 Sum : %ld\n",sum);
		MPI_Recv(&sum, 1, MPI_LONG, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		global_sum+=sum;
		printf(" >>> Slave2 Sum : %ld\n",sum);
		MPI_Recv(&sum, 1, MPI_LONG, 3, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		global_sum+=sum;
		printf(" >>> Slave3 Sum : %ld\n",sum);
		MPI_Recv(&sum, 1, MPI_LONG, 4, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		global_sum+=sum;
		printf(" >>> Slave4 Sum : %ld\n",sum);
		printf(" >>> Global Sum : %ld\n",global_sum);
	}
	else if(rank == 1){
		printf(" >>> Slave1 started :  [ OK ]\n");
		long i,sum = 0;
		for( i = 1 ; i <= 100 ; i++)
			sum+= i*i;
		MPI_Send(&sum, 1, MPI_LONG, 0, 1, MPI_COMM_WORLD );
		printf(" >>> Slave1 -> SENT :  [ OK ]\n");
	}
	else if(rank == 2){
		printf(" >>> Slave2 started :  [ OK ]\n");
		long i,sum = 0;
		for( i = 101 ; i <= 200 ; i++)
			sum+= i*i;
		MPI_Send(&sum, 1, MPI_LONG, 0, 2, MPI_COMM_WORLD );
		printf(" >>> Slave2 -> SENT :  [ OK ]\n");
	}
	else if(rank == 3){
		printf(" >>> Slave3 started :  [ OK ]\n");
		long i,sum = 0;
		for( i = 201 ; i <= 300 ; i++)
			sum+= i*i;
		MPI_Send(&sum, 1, MPI_LONG, 0, 3, MPI_COMM_WORLD );
		printf(" >>> Slave3 -> SENT :  [ OK ]\n");
	}
	else if(rank == 4){
		printf(" >>> Slave4 started :  [ OK ]\n");
		long i,sum = 0;
		for( i = 301 ; i <= 400 ; i++)
			sum+= i*i;
		MPI_Send(&sum, 1, MPI_LONG, 0, 4, MPI_COMM_WORLD );
		printf(" >>> Slave4 -> SENT :  [ OK ]\n");
	}


	return 0;
}
