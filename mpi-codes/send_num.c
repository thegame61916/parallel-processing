#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
	  int numprocs, rank, namelen;
	  	int num;
	    char processor_name[MPI_MAX_PROCESSOR_NAME];

	      MPI_Init(&argc, &argv);
	        MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
		  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
//		      printf("Process %d on %s out of %d -> SERVER\n", rank, processor_name, numprocs);
		    MPI_Get_processor_name(processor_name, &namelen);
		/*if(rank == 0){

			MPI_Send(&rank, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

		      printf("Process %d on %s out of %d -> SERVER\n", rank, processor_name, numprocs);
		     }
		else{
			MPI_Recv( &rank, 1, MPI_INT,1, 0, MPI_COMM_WORLD, NULL);
		      printf("Process %d on %s out of %d  CLIENT\n", rank, processor_name, numprocs);
		}*/
		    int number;
		      if (rank == 0) {
			      number = 1000;
				      MPI_Send(&number, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
			      number = 300;
				      MPI_Send(&number, 1, MPI_INT, 2, 0, MPI_COMM_WORLD);
			      number = 20;
				      MPI_Send(&number, 1, MPI_INT, 3, 0, MPI_COMM_WORLD);
			      number = 5;
				      MPI_Send(&number, 1, MPI_INT, 4, 0, MPI_COMM_WORLD);
				        } else {
						    MPI_Recv(&num, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,
								                 MPI_STATUS_IGNORE);
						        printf("Process %d received number %d from process 0\n",rank, num);
							  }
		
		        MPI_Finalize();
}

