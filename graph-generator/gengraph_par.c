#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#define BUF 100
static int st = 31;
/*
 * randint() function is called to generate random capacities for different arcs
 */

int randint(int max)  
{ 
	int min = 1;
	srand(time(NULL) - st );
	st *= 313;
	return min+(int)((int)(max-min+1)*(rand()/(RAND_MAX+1.0))); 
}  

int main(int argc, char* argv[] )
{
	/*
	 * Initializing MPI
	`*/
	
	int numprocs, rank, namelen;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &rank);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(processor_name, &namelen);
    
    /*
     * Implementing a static code for 9 vertices to distribute 2 vertices per slave 	* node
     * Basically we need the upper trianglular matrix 
     */
	int e = (9 * (9 - 1)) / 2;
	int c = 900;
	int s = 1;
	int v = 9;
   	/*
	 * Implementing Parallel code :
	*/
	if( rank == 0){
		/*epsilon*/
		printf("Master Begin\n");
		//FILE *f = fopen("demo.txt","rw");
		int i,j;
		//fprintf(f,"%d\t%d\n",v,e);
		//MPI_RECV 
		char buf1[100];
		char buf2[100];
		char buf3[100];
		char buf4[100];
		char buf[100];
		//buf[0] = '\0';
		MPI_Recv(buf1, BUF, MPI_CHAR, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		strcat(buf,buf1);
		MPI_Recv(buf2, BUF, MPI_CHAR, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		strcat(buf,buf2);
		MPI_Recv(buf3, BUF, MPI_CHAR, 3, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		strcat(buf,buf3);
		MPI_Recv(buf4, BUF, MPI_CHAR, 4, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
		strcat(buf,buf4);
		printf("%s\n",buf);
		//fputs(buf,f);
		//fclose(f);
	} 
	if( rank == 1 ){
		/*alpha*/
		printf("Alpha begin\n");
		int i,j;
		char buf[100];
		buf[0] = '\0';
		for (i = 1 ; i <= 2 ; i++)
		for(j = i+1 ; j <= v ; j++ ){
			char str[6];
			sprintf(str,"%d\t%d\t%d\n",i,j,randint(c));
			strcat(buf,str);
		}
		//MPI_SEND
		MPI_Send(buf,BUF, MPI_CHAR, 0, 1, MPI_COMM_WORLD);
	}
	if( rank == 2 ){
		/*beta*/
		printf("beta begin\n");
		int i,j;
		char buf[100];
		buf[0] = '\0';
		for (i = 3 ; i <= 4 ; i++)
		for(j = i+1 ; j <= v ; j++ ){
			char str[6];
			sprintf(str,"%d\t%d\t%d\n",i,j,randint(c));
			strcat(buf,str);
		}
		//MPI_SEND
		MPI_Send(buf,BUF,MPI_CHAR,0,2,MPI_COMM_WORLD);
	}
	if( rank == 3 ){
		/*gamma*/
		printf("Gamma begin\n");
		int i,j;
		char buf[100];
		buf[0] = '\0';
		for (i = 5 ; i <= 6 ; i++)
		for(j = i+1 ; j <= v ; j++ ){
			char str[6];
			sprintf(str,"%d\t%d\t%d\n",i,j,randint(c));
			strcat(buf,str);
		}
		//MPI_SEND
		MPI_Send(buf,BUF,MPI_CHAR,0,3,MPI_COMM_WORLD);
	}
	if( rank == 4 ){
		/*delta*/
		printf("Delta begin\n");
		int i,j;
		char buf[100];
		buf[0] = '\0';
		for (i = 7 ; i <= 8 ; i++)
		for(j = i+1 ; j <= v ; j++ ){
			char str[6];
			sprintf(str,"%d\t%d\t%d\n",i,j,randint(c));
			strcat(buf,str);
		}
		//MPI_SEND
		MPI_Send(buf,BUF,MPI_CHAR,0,4,MPI_COMM_WORLD);
	} 
    
	//return 0;
	MPI_Finalize();
}


