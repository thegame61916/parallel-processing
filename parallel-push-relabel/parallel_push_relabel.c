#include "parallel_push_relabel.h"

int **alloc_2d_int(int rows, int cols) {
    int *data = (int *)calloc(rows*cols,sizeof(int));
    int **array= (int **)calloc(rows,sizeof(int*));
    int i;
    for ( i=0; i<rows; i++)
        array[i] = &(data[cols*i]);

    return array;
}

void print_mat(int **mat) {
    int i, j;
    
    fprintf(fo,"------------------------------\n");

    for (i = 0; i < num_vertices; i++) {
        for (j = 0; j < num_vertices; j++)
            if (mat[i][j] > 0) {
                fprintf(fo, "from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    fprintf(fo, "\n");
    fprintf(fo,"------------------------------\n\n");
}

void print_matrix(int **mat) {
    int i, j;
    
    printf("------------------------------\n");

    for (i = 0; i < num_vertices; i++) {
        for (j = 0; j < num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
    printf("------------------------------\n\n");
}


//to provide initial preflow from source to connected vertices
void preflow(int **c, int **f, int *excess, int source){
    
    for (int i = 0; i < num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            f[i][source] -= c[source][i];
            excess[i]     = c[source][i];
            excess[source] -= c[source][i];
        }
    }
}

//push operation is easy to understand
int push(int **c, int **f, int *excess, int u, int v){
    int push_flow = MIN(excess[u], c[u][v] - f[u][v]);
    f[u][v]   += push_flow;
    f[v][u]   -= push_flow;
    excess[u] -= push_flow;
    excess[v] += push_flow;

    return push_flow;
}

//relabeling operation will increase the height of the
//active vertex w.r.t the connected vertex with minimum height
//and for which the edege is following capacity and flow constraints
//I understood relabeling from here 
//https://www.dropbox.com/s/c13pyiwqajyvmg3/Maximum_flow%20%281%29.ppt

void relabel(int **c, int **f, int *height, int u){

    int min_height = INFINITE;
    for (int v = 0; v < num_vertices; v++) {
        if (c[u][v] - f[u][v] > 0) {
            min_height = MIN(min_height, height[v]);
            height[u] = min_height + 1;
        }
    }
}

//required push and relabel operations are performed in this function
//as shown all the vertices connected to active vertex are checked for
//push of flow. Height constraints are also included.
void send_flow(int **c, int **f, int *excess, int *height, int u){
    int v = 0, sent;
    while(excess[u] > 0){
        //printf("%s %d\n", "Checkpoint : Iterating in sendflow() ", u);
        if (v < num_vertices) {
            if (( c[u][v] - f[u][v] > 0 ) && ( height[u] > height[v] )) {

                fprintf(fo, "Active vertex found : %d\n", u);
                fprintf(fo, "Relabeled %d to : %d\n", u, height[u]);
                sent = push(c, f, excess, u, v);
                fprintf(fo, "%d-[%d]->%d\n", u, sent, v);

            }
            v ++;
        }else {
            relabel(c, f, height, u);
            v = 0;
        }
    }
}

//function is called to reinitialize the active vertex routine
//in this the processed vertex brought to front and the remaining 
//vertices are checked again for activeness in further routine
void rearrange(int k, int *intermediate){
    int temp = intermediate[k];
    for (int j = k; j > 0 ; j--) {
        intermediate[j] = intermediate[j - 1];
    }
    intermediate[0] = temp;
}

int push_relabel(int **c, int **f, int *excess, int source, int sink){
    printf("%s\n", "Checkpoint : push_relabel() begins");
    int i, j = 0, old_height, sum = 0;
    num_vertices = sink + 1;
    /*
     * Commentting excess and passing it as parameter from slave 3
     */
    //int *excess       = (int *)calloc(num_vertices, sizeof(int));
    int *height       = (int *)calloc(num_vertices, sizeof(int));
    int *intermediate = (int *)calloc(num_vertices, sizeof(int));
    fo = fopen("output_part2.txt","w");
    //printf("%s%d\n", "Checkpoint : num_vertices : ", num_vertices);
    printf("%s\n", "Checkpoint : File Open");
    //initializing with preflow
    /*
     * Commentting preflow method which is being implemented by slave 2
     */
    //preflow(c, f, excess, source);

    //initializing the height of source
    height[source] = num_vertices;

    //getting intermediate nodes for processing
    for (i = 0; i < num_vertices; i++) {
        if (i != 0 && i != num_vertices - 1) {
            intermediate[i - 1] = i;
        }
    }

    printf("%s\n", "Checkpoint : intermediate nodes processed");
    //performing operations considering single intermediate vertex at a time
    fprintf(fo, "Format : from-[sent flow]->to\n");
    
    for (i = 0; i < num_vertices - 2; i++) {
        int u = intermediate[i];
        old_height = height[u];
        //function will check if the provided vertex is active or not.
        //in the case of active it will push the flow accordingly
        //printf("%s %d\n", "Checkpoint : Iterating ",i);
        send_flow(c, f, excess, height, u);
        //when old_height is lesser than the new one the active vertex has been found,
        //the graph's flow has been changed and we need to reinitialize the the algorithm
        //because there is a possibility that the vertex which has been processed earlier
        //may become active again.
        if (old_height < height[u]) {
            rearrange(i, intermediate);
            i = 0;
        }
    }
    printf("%s\n", "Checkpoint : Height Sent");

    MPI_Send(&height[0], num_vertices, MPI_INT, 0, 2, MPI_COMM_WORLD );
    
    
}

// adding function parameter f, to pass file name to master_job() method
void master_job(char *f) {
    clock_t tic = clock();   

    int **flow, **capacities, *height, i, from, to, linenumber = 1, capacity;
    
    if (f == NULL) {
        printf("Format is : ./push-relabel <graphfile>\n");
        exit(0);
    }else {

    /*
     * Sending input filename to slave 1 for reading
     */
     MPI_Send(&f[0],9,MPI_CHAR, 1, 1, MPI_COMM_WORLD );
        //output file
        fo = fopen("output.txt","w");
    }
    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    flow = alloc_2d_int(num_vertices,num_vertices);
    capacities = alloc_2d_int(num_vertices,num_vertices);

    MPI_Recv(&capacities[0][0], num_vertices*num_vertices, MPI_INT, 1, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Master has received capacities!" );
    print_matrix(capacities);

    //printing the capacities of the matrix
    fprintf(fo, "\nCapacities:\n");
    print_mat(capacities);

    height = (int *)calloc(num_vertices, sizeof(int));
    MPI_Recv(&height[0], num_vertices, MPI_INT, 3, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    //final labels on the vertices
    fprintf(fo, "\nFinal heights : \n");
    fprintf(fo, "------------------------------\n");
    
    for (i = 0; i < num_vertices; i++) {
        fprintf(fo, "Height[%d] : %d\n", i, height[i]);
    }

    fprintf(fo, "------------------------------\n");
    //Maximum flow obtained at the sink
    int sum = 0;
    MPI_Recv(&flow[0][0], num_vertices*num_vertices, MPI_INT, 3, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    print_matrix(flow);
    for (i = 0; i < num_vertices; i++) {
        if (flow[i][num_vertices - 1] > 0) {
            sum += flow[i][num_vertices - 1];
        }
    }

    //printing the final flow in the network
    fprintf(fo, "\nFlows:\n");
    print_mat(flow);

    fprintf(fo, "Maximum flow : %d\n", sum);
    printf("Maximum flow : %d\n", sum);
    fprintf(fo,"------------------------------\n");
    clock_t toc = clock();
    fprintf(fo, "Execution time : %f seconds\n", (double)(toc - tic)/1000000);
    fprintf(fo,"------------------------------\n");
}

void slave_one_job() {

    int **capacities, i, from, to, linenumber = 1, capacity;

    char *f = (char *)calloc(9, sizeof(char));
    MPI_Recv(&f[0], 9, MPI_CHAR, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    
     FILE *fp = fopen(f,"r");
    //getting number of vertices and edges
    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);
    printf("Number of vertices at slave 1 : %d\n", num_vertices);
    //initializing the capacities
    capacities = alloc_2d_int(num_vertices,num_vertices);
    

    //getting the info about capacities and edges
    for (i = 0; i < num_edges; i++) {

        fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
        capacities[from][to] = capacity;
        /*
         * Once the capacities of edges leaving the source vertex are read from the input file
         * the array of those capacities can be sent to slave immidiately to calculate preflow
         */
        if ( i == num_vertices - 2) {
            MPI_Send(&num_vertices, 1, MPI_INT, 2, 1, MPI_COMM_WORLD);
            MPI_Send(&num_vertices, 1, MPI_INT, 0, 5, MPI_COMM_WORLD);
            MPI_Send(&num_vertices, 1, MPI_INT, 4, 5, MPI_COMM_WORLD);
            MPI_Send(&capacities[from][0], num_vertices, MPI_INT, 2, 2, MPI_COMM_WORLD);
            MPI_Send(&capacities[from][0], num_vertices, MPI_INT, 4, 2, MPI_COMM_WORLD);
        }
    }
    /*
    * TRIED FORMING A BACKUP ARRAY OF CAPACITIES TO MAKE CALLS USING SEPERATE ARRAYS
    */
    int **capacities_backup;
    capacities_backup = alloc_2d_int( num_vertices, num_vertices);
    for (int i = 0; i < num_vertices; ++i)
    {
        for (int j = 0; j < num_vertices; ++j)
        {
            capacities_backup[i][j] = capacities[i][j];
        }
    }

    /*
     * THIS IS THE MPI_SEND CALL TO SEND CAPACITIES TO SLAVE 3 WITH TAG 4, WITH NO ERRORS
     */
    MPI_Send(&capacities[0][0], num_vertices * num_vertices , MPI_INT, 3, 4, MPI_COMM_WORLD);
    printf("%s\n", "MPI_Send success!");
    MPI_Send(&capacities_backup[0][0], num_vertices*num_vertices, MPI_INT, 0, 3, MPI_COMM_WORLD);

/**********************************************************************************/
    
}

void slave_two_job() {
    int **flow;

    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Number of vertices at slave 2 : %d\n", num_vertices);
    flow       = alloc_2d_int(num_vertices, num_vertices);
    int *c = (int *) calloc( num_vertices, sizeof(int));
    //int *excess = (int *) calloc( num_vertices, sizeof(int));

    MPI_Recv(&c[0], num_vertices, MPI_INT, 1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    
    for (int i = 0; i < num_vertices; i++) {
        if (c[i] != 0 && i != 0) {
            flow[0][i] += c[i];
            flow[i][0] -= c[i];
            //excess[i]  = c[i];
            //excess[0] -= c[i];
        }
    }
    printf("Number of vertices before sending at slave 2 : %d\n", num_vertices);
    MPI_Send(&num_vertices, 1, MPI_INT, 3, 1, MPI_COMM_WORLD);
    MPI_Send(&flow[0][0], num_vertices*num_vertices, MPI_INT, 3, 2, MPI_COMM_WORLD);
    //MPI_Send(&excess[0], num_vertices, MPI_INT, 3, 3, MPI_COMM_WORLD);
    

}

void slave_three_job() {

    int **flow, *excess;
    int **capacities;
    MPI_Recv(&num_vertices, 1, MPI_INT, 2, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("Number of vertices at slave 3 : %d\n", num_vertices);

    
    capacities = alloc_2d_int( num_vertices, num_vertices);
    flow = alloc_2d_int(num_vertices,num_vertices);
    excess = (int *)calloc(num_vertices, sizeof(int));

    MPI_Recv(&excess[0], num_vertices, MPI_INT, 4, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Slave 3 has received excess");
    
    MPI_Recv(&flow[0][0], num_vertices*num_vertices, MPI_INT, 2, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Slave 3 has received flow");
    print_matrix(flow);

    /*
     * THIS IS WHERE THE MPI_Recv ERROR OCCURS :'(
     * IF U COMMENT THE FOLLOWING FOUR LINES THE ENTIRE EXECUTION HALTS EXPECTING A MESSAGE, BUT THERE ARE NO ERRORS
     */
    MPI_Recv(&capacities[0][0], num_vertices * num_vertices, MPI_INT, 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
    printf("%s\n", "Slave 3 has received capacities");
    print_matrix(capacities);

    push_relabel(capacities, flow, excess, 0, num_vertices - 1);
    MPI_Send(&flow[0][0], num_vertices*num_vertices, MPI_INT, 0, 1, MPI_COMM_WORLD);
    
}

void slave_four_job() {
    MPI_Recv(&num_vertices, 1, MPI_INT, 1, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("Number of vertices at slave 2 : %d\n", num_vertices);
    int *c = (int *) calloc( num_vertices, sizeof(int));
    int *excess = (int *) calloc( num_vertices, sizeof(int));
    MPI_Recv(&c[0], num_vertices, MPI_INT, 1, 2, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    for (int i = 0; i < num_vertices; i++) {
        if (c[i] != 0 && i != 0) {
            excess[i]  = c[i];
            excess[0] -= c[i];
        }
    }
    MPI_Send(&excess[0], num_vertices, MPI_INT, 3, 3, MPI_COMM_WORLD);

}


int main(int argc, char *argv[])
{
    int numprocs, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    switch(rank) {
        case 0 :
            printf("I am Master\n");
            master_job(argv[1]);
        break;

        case 1 :
            printf("I am Slave %d \n",rank);
            slave_one_job();
        break;

        case 2 :
            printf("I am Slave %d \n",rank);
            slave_two_job();
        break;

        case 3 :
            printf("I am Slave %d \n",rank);
            slave_three_job();
        break;

        case 4 :
            printf("I am Slave %d \n",rank);
            slave_four_job();
        break;
    }

    MPI_Finalize();
    return 0;
}
