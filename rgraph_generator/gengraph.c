#include<stdio.h>
#include<stdlib.h>
static int ste = 31;

/*
 * randint() function is called to generate random capacities for different arcs
 */

int randint(int max)  
{ 
	int min = 2;
	srand(time(NULL) - ste );
	ste *= 313;
	return min+(int)((int)(max-min+1)*(rand()/(RAND_MAX+1.0))); 
}  

int main(int argc, char* argv[] )
{
    /*
     * Checking whether the user has passed correct number of arguments
     */
	if(argc != 11){
		printf(" Format Error : exit(0)\n Usage: ./gen -v <vertices> -c <capacity> -s <start-vertex> -st <stages> -f <filename> \n ");
		exit(0);
	}

	FILE *f = fopen(argv[10],"w");
	int i,j;
	int v  = atoi(argv[2]);
	int st = atoi(argv[8]);
	
	int c = atoi(argv[4]);
	int s = atoi(argv[6]);

	int **graph;
	int limit = v + 1;

	graph = (int **) calloc(limit, sizeof(int *));

	for (i = s; i < limit; ++i){
		
		graph[i] = (int *) calloc(limit, sizeof(int));
	}


    /*
     *Count variable to count the total number of arcs
     */
	int counter = 0;
		
	if (v - 2 < st || st == 0){
		
		printf("\nInvalid number of stages\nEXIT[0]\n");
		exit(0);
	}

	for (i = 1; i <= st; ++i){

		graph[1][1 + i] = randint(c);
		counter ++;
		for (j = 1 + i; j < v; j = j + st){
			
			if (j + st < v){
			graph[j][j + st] = randint(c);
			counter ++;
			}	
		}
		graph[j - st][v] = randint(c);
		counter ++;
	}

	//adding random arcs in the graph
	int ran_arc_num = v / 4;
	int a, b, from, to;
	for (i = 0; i < ran_arc_num; ++i){
		
		do{
			a = randint(v - 1);
			b = randint(v - 1);
		}while(a == b);

		if (a < b){

			from = a;
			to 	 = b;
		}else{

			from = b;
			to   = a;
		}
		
		if (graph[from][to] == 0){
			graph[from][to] = randint(c);
			counter ++;
		}
	}
		
	//adding first line in the output file
	fprintf(f,"%d\t%d\n",v,counter);

	//generating the graph files with respect the source vertex
	for (i = 1; i < limit; ++i){
		
		for (j = 1; j < limit; ++j){
			
			if (graph[i][j] != 0){
				if (s == 1)
				{
					fprintf(f,"%d\t%d\t%d\n", i, j,	graph[i][j]);				
				}else{

					fprintf(f,"%d\t%d\t%d\n", i - 1, j - 1,	graph[i][j]);				
				}
			}
		}
	}
	
	printf("\n File created : %s \t[ OK ]\n ARC COUNT : %d \t[ OK ]\n\n",argv[10],counter);
	fclose(f);
	return 0;
}


