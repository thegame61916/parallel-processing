#include<stdio.h>
#include<stdlib.h>
static int st = 31;
int myrandom( int m ) 
{
	int a,c,seed;
	seed = time(NULL);
	a = time(NULL) * seed;
	c = a - (time(NULL)  % 10);
	return (int)((abs((a *seed - c)) % m ) + 1 );
}
int randint(int max)  
{ 
	int min = 0;
	srand(time(NULL) - st );
	st *= 313;
	return min+(int)((int)(max-min+1)*(rand()/(RAND_MAX+1.0))); 
}  

int main(int argc, char* argv[] )
{
	if(argc != 9){
		printf(" Format Error : exit(0)\n Usage: ./gen -v <vertices> -c <capacity> -s <start-vertex> -f <filename> \n ");
		exit(0);
	}
	FILE *f = fopen(argv[8],"w");
	int i,j;
	int v = atoi(argv[2]);
	int e = (v * (v - 1)) / 2;
	int c = atoi(argv[4]);
	int s = atoi(argv[6]);
	fprintf(f,"%d\t%d\n",v,e);
	int count = 0;
	if( s == 1 ){
		for( i = 1 ; i < v ; i++ ){
			for ( j = i ; j <= v ; j++ ){
				if( i != j ){
					fprintf(f,"%d\t%d\t%d\n",i,j,randint(c));
					count++;
				}
			}
		}
	} else {
		for( i = 0 ; i < v-1 ; i++ ){
			for ( j = i ; j <= v - 1 ; j++ ){
				if( i != j ){
					fprintf(f,"%d\t%d\t%d\n",i,j,randint(c));
					count++;
				}
			}
		}
	}
	printf("\n File created : %s \t[ OK ]\n ARC COUNT : %d \t[ OK ]\n\n",argv[8],count);
	fclose(f);
	return 0;
}


