
#include "graphread.h"
#define MAX_VERTEX 53600000
#define MAX_EDGES 53600000
#define MAX_FLOW 53600000
#define SOURCE 1
#define MIN(X,Y) X < Y ? X : Y

int i, vertices, edges;
ARC *arcs[MAX_EDGES];
VERTEX *vertex[MAX_VERTEX];


void read(char *fname) {

    FILE *fp;
    fp = fopen(fname, "r");   
    fscanf(fp, "%d\t%d", &vertices, &edges);
    /* creating an array of ARCS to store all the edges*/
    int t_from, t_to, t_capacity;

    for (i = 1;i <= edges ; i++ ) {
        t_from  = 0;
        t_to    = 0;
        t_capacity = 0;
        fscanf(fp, "%d\t%d\t%d", &t_from, &t_to, &t_capacity);
        arcs[i] = (ARC *)malloc(sizeof(struct arc)); 
        arcs[i] -> from     = t_from;
        arcs[i] -> to       = t_to;
        arcs[i] -> capacity = t_capacity;
        arcs[i] -> flow     = 0;
        printf(" %d --(%d/%d)--> %d\n", arcs[i]-> from, arcs[i]->flow, arcs[i] -> capacity, arcs[i] -> to);
    }
    /* creating an array of VERTEX to store all the vertices*/

    printf( "\n vertex( value , label )\n" );
    for( i = 1; i <= vertices ; i++){
        vertex[i] = (VERTEX *)malloc(sizeof(struct vertex));
        vertex[i] -> value  = i;

        if( i == SOURCE )
            vertex[i] -> label  = vertices;
        else 
            vertex[i] -> label  = 0;
        printf(" vertex( %d , %d )\n",vertex[i]->value,vertex[i]->label);
    }
    printf("\n Graph read \t\t [ OK ]\n");
    printf(" Initial labelling \t [ OK ]\n");
    fclose(fp);

}
void display(){
    printf("\n Displaying ARCS >>>\n");
    for (i = 1; i <= edges; i++) 
        printf(" %d --(%d/%d)--> %d\n", arcs[i]-> from, arcs[i]->flow, arcs[i] -> capacity, arcs[i] -> to);
        
}

void pre_flow(){
    for( i = 1 ; i <= edges ; i++ )
        if(arcs[i]->from == SOURCE ){
            arcs[i]->flow = arcs[i]->capacity;
            vertex[arcs[i]->to]->excess = arcs[i]->capacity;
        }
    printf(" Preflow \t\t [ OK ]\n");
    display();
}

int isActive(VERTEX* v){
    /*Checking if it has an edge where it can push excess flow*/
    if(v->excess != 0){
        int i;
        for(i=1; i<=edges; i++)
            if(arcs[i]->from == v->value)
                if((arcs[i]->capacity - arcs[i]->flow) != 0)
                    return 1;
    }
    return 0;
}

int active_vertex_exists() {
    int i=0;
    for (i = 2; i <= vertices; i++)
        if( isActive(vertex[i]) )
            return 1;
    return 0;
}

void compute(){
    int i,j,min;
    ARC* e;
    while(active_vertex_exists()) {
       for (i = 2; i < vertices; i++) {
           printf("\n Checking vertex : %d\n",i);
           if(isActive(vertex[i])){
                /*Getting a outgoing edge from this vertex*/
                printf("\n Processing vertex : %d\n",vertex[i]->value);
                e = NULL;
                for( j=1; j <= edges; j++){
                    if(arcs[j]->from == vertex[i]->value && (arcs[j]->capacity-arcs[j]->flow) != 0) {
                        e = arcs[j];
                        break;
                    }
                }
                printf(" ARC found : %d --> %d \n",e->from,e->to);
                if(vertex[e->from]->label <= vertex[e->to]->label)
                    vertex[e->from]->label = vertex[e->to]->label + 1;
                min = MIN( (e->capacity)-(e->flow) , vertex[e->from]->excess);
                e->flow += min;
                vertex[e->from]->excess -= min;
                vertex[e->to]->excess += min;
                printf(" Excess(%d) = %d\n",vertex[e->from]->value,vertex[e->from]->excess);
                printf(" %d --(%d/%d)--> %d\n",e->from,e->flow,e->capacity,e->to);
                printf(" [ OK ]\n");
           }
       }
           
    }

    printf("\n------------------------------------\n Excess flow on vertices >>> \n");
    for (i = 2; i < vertices; i++) {
        printf(" Excess(%d,%d)\n",i,vertex[i]->excess);
    }
    printf("\n------------------------------------\n Label on vertices >>> \n");
    for (i = 1; i <= vertices; i++) {
        printf(" Vertex(%d,%d)\n",i,vertex[i]->label);
    }

    printf("\n------------------------------------\n Pushing excess flow back to source >>>\n NOTE: Follow the arrows to trace the path of the excess flow back to source \n\n");
    for (i = vertices - 1; i > 1; i--) {
        if(vertex[i]->excess != 0){
            for (j = 1; j <= edges; j++) {
                if(arcs[j]->to == vertex[i]->value && vertex[i]->excess != 0 && arcs[j]->flow != 0 ){
                    if( vertex[arcs[j]->to]->label <= vertex[arcs[j]->from]->label ) {
                        vertex[arcs[j]->to]->label = vertex[arcs[j]->from]->label +1;
                        printf(" Relabelling vertex %d to %d \n",vertex[arcs[j]->to]->value,vertex[arcs[j]->to]->label);
                    }
		    if(arcs[j]->capacity <= vertex[arcs[j]->to]->excess){
	                    printf(" Pushing %d units : %d --> %d\n",arcs[j]->capacity,vertex[arcs[j]->to]->value,vertex[arcs[j]->from]->value);
			    vertex[arcs[j]->from]->excess += arcs[j]->capacity;
			    arcs[j]->flow = 0;
			    vertex[arcs[j]->to]->excess -= arcs[j]->capacity;
		    } else {
	                    printf(" Pushing %d units : %d --> %d\n",vertex[i]->excess,vertex[arcs[j]->to]->value,vertex[arcs[j]->from]->value);
        	            vertex[arcs[j]->from]->excess += vertex[arcs[j]->to]->excess;
                	    arcs[j]->flow -= vertex[arcs[j]->to]->excess;
	                    vertex[arcs[j]->to]->excess = 0;
        	            break;
		    }
                }
            }
        }
    }
    
    printf("\n------------------------------------\n Final Excess flow on vertices >>> \n");
    for (i = 2; i < vertices; i++) {
        printf(" Excess(%d,%d)\n",i,vertex[i]->excess);
    }
}

int final_verify(){
	int s = 0, t = 0,i = 0;
	for (i = 1; i <= edges; i++) {
		if(arcs[i]->from == SOURCE)
			s += arcs[i]->flow;
		if(arcs[i]->to == vertices)
			t += arcs[i]->flow;
	}
	if( s == t )
		printf("\n------------------------------------\n Cut of SOURCE = Cut of SINK = %d\n The solution is legit and hence optimal\n",s);
	else 
		printf("\n------------------------------------\n The solution is not optimal\n");
}

int main(int argc, char *argv[])
{
    /*Reading the graph from the file
     *getting number of vertices and edges
     */
    read(argv[1]);
    /*calling method to initiate preflow*/
    pre_flow();
    compute();
    display();
    final_verify();
    return 0;
}
