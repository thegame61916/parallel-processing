#include "para_epsilon.h"

// Printing the matrix values
void print_mat(int **mat) {
    int i, j;
    
    printf("------------------------------\n");

    for (i = 1; i <= num_vertices; i++) {
        for (j = 1; j <= num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
    printf("------------------------------\n\n");
}

// getting the column of a matrix
int *get_mat_column(int index, int **mat, int *temp){

    int count = 1;
    for (int i = 0; i < num_vertices; ++i){
        
        temp[i] = mat[count][index];
        count ++;
    }

    return temp;
}

//getting the column sum for particular index of the matrix
int get_mat_column_sum(int index, int **mat){

    int sum = 0;
    for (int i = 1; i <= num_vertices; ++i){
        
        sum += mat[i][index];
    }

    return sum;
}

/*--------------CODE OF ACTUAL SERIAL ALGO---------------------*/
//to provide initial preflow from source to connected vertices  
void preflow(int **c, int **f, int source){
    
    for (int i = 1; i <= num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            //surplus[i]     = c[source][i];
            //surplus[source] -= c[source][i];
        }
    }
}


int delta_push(int **c, int **f, int *surplus, int u, int v){
    int push_flow = MIN(surplus[u], c[u][v] + f[v][u]);
    surplus[u] -= push_flow;
    surplus[v] += push_flow;
    f[v][u]   -= push_flow;

    return push_flow;
}


int push(int **c, int **f, int surplus, int u, int v){
    int push_flow = MIN(surplus, c[u][v] - f[u][v]);

    f[u][v]   += push_flow;

    return push_flow;
}

//generating push-list containing the admissible  arcs
void generate_pushlist(int vertex, int *price, int **neighbour, int **admissible, int **f, int **c){
    
    int j, index, nvertex;
        
    j = 1;
    index = 1;
    while(neighbour[vertex][j] != 0){

        nvertex = neighbour[vertex][j];

        //e+ balanced arcs condition
        if ((price[vertex] == price[nvertex] + EPSILON) && (f[vertex][nvertex] < c[vertex][nvertex])) {
            admissible[vertex][index] = nvertex;
            index ++;
        }else if((c[vertex][nvertex] == 0) && (f[nvertex][vertex] > LB)) {
            admissible[vertex][index] = nvertex;
            index ++;
        }
        j ++;
    }

}


int up_iteration(int **c, int **f, int *surplus, int source, int sink){

    int i, j = 0, sum = 0, count = 0, push_index;
    int *price        = (int *)calloc(num_vertices + 1, sizeof(int));
    
    int **admissible   = (int **)calloc(num_vertices + 1, sizeof(int));
    int **neighbour    = (int **)calloc(num_vertices + 1, sizeof(int));

    for (i = 1; i <= num_vertices; i++) {
        admissible[i] = (int *)calloc(num_vertices + 1, sizeof(int));
        neighbour[i]  = (int *)calloc(num_vertices + 1, sizeof(int));
    }

    //initializing the price of source
    price[source]   = 1;
    surplus[source] = INFINITE;

    for (i = 1; i <= num_vertices; i++) {

        //getting the neighbours
        count = 1;
        for (j = 1; j <= num_vertices; j++) {
            if (( c[i][j] != 0 ) || ( c[j][i] != 0)) {
                neighbour[i][count] = j;
                count ++;
            }
        }
    }

    //generating the pushlist for incedent arcs and discharging respective surpluses
    for (i = 2; i < num_vertices; i++) {
        push_index = 1;
        if (surplus[i] > 0) {
            generate_pushlist(i, price, neighbour, admissible, f, c);
            if (surplus[i] > 0) {
                //print_admissible(admissible);

                printf("Admissible edge is %d", admissible[i][push_index]);
                surplus[i] -= delta_push(c, f, surplus, i, admissible[i][push_index]);
            
                
                printf("Push is performed and surplus of %d is : %d\n", i, surplus[i]);
                push_index ++;
                i = 1;
            }
        }
    }

    printf( "format : from-[sent flow]->to\n");
    

    //final labels on the vertices
    printf( "\nfinal prices: \n");
    printf( "------------------------------\n");
    
    for (i = 1; i <= num_vertices; i++) {
        printf( "price[%d] : %d\n", i, price[i]);
        printf( "surplus[%d] : %d\n", i, surplus[i]);
    }

    printf( "------------------------------\n");
    //maximum flow obtained at the sink
    
    for (i = 1; i <= num_vertices; i++) {
        if (f[i][num_vertices] > 0) {
            sum += f[i][num_vertices];
        }
    }
    printf( "maximum flow : %d\n", sum);
    
}

/*-------------------------------------------------------------*/

int main(int argc, char const *argv[])
{
    clock_t tic = clock();

    MPI_Init(NULL, NULL);
    
    int size, rank;
    
    MPI_Request reqs[num_vertices];
    MPI_Status stats[num_vertices];
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    // Master

    if (rank == 0){
        
        FILE *fp;
        if (argv[1] == NULL) {
            printf("Format is : ./push-relabel <graphfile>\n");
            exit(0);
        }else {
            //output file
            //fo = fopen("output.txt","w");
            //input file
            fp = fopen(argv[1], "r");   
        }


        //getting number of vertices and edges
        fscanf(fp, "%d\t%d", &num_vertices, &num_edges);

        //initializing the flows and the capacities
        flow       = (int **) calloc(num_vertices + 1, sizeof(int*));
        capacities = (int **) calloc(num_vertices + 1, sizeof(int*));

        for (i = 1; i <= num_vertices; i++) {
            flow[i]       = (int *) calloc(num_vertices + 1, sizeof(int));
            capacities[i] = (int *) calloc(num_vertices + 1, sizeof(int));
        }

        //getting the info about capacities and edges
        for (i = 1; i <= num_edges; i++) {

            fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
            capacities[from][to] = capacity;
        }
        
        //printing the capacities of the matrix
        printf("\nCapacities:\n");
        print_mat(capacities);

        preflow(capacities, flow, 1);

        printf("\nAfter preflow:\n");
        print_mat(flow);

        //serializing the capacities 2d array for the purpose of broadcasting
        int index = 0;
        
        capacities_copy = (int *)malloc(sizeof(int) * num_vertices * num_vertices);
        flow_copy       = (int *)malloc(sizeof(int) * num_vertices * num_vertices);

        for (int i = 1; i <= num_vertices; i++) {
            for (int j = 1; j <= num_vertices; j++) {
                capacities_copy[index] = capacities[i][j];
                flow_copy[index]       = flow[i][j];
                index ++;
            }
        }

        //distributing job
        for (int i = 0; i < size - 2; ++i){
            
            int temp[num_vertices];
            inqueue  = get_mat_column(i + 2, flow, temp);
            outqueue = flow[i + 2] + 1;

            // sending inqueue to the respective processes
            MPI_Send(inqueue, num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

            // sending outqueue to the respective processes
            MPI_Send(outqueue, num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

            // sending capacities to all the processes
            MPI_Send(capacities_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
            MPI_Send(flow_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

       }

     
    }

    // broadcasting number of vertices and edges to the other processes.
    MPI_Bcast(&num_vertices, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_edges, 1, MPI_INT, 0, MPI_COMM_WORLD);
    //MPI_Bcast(capacities_copy, num_vertices * num_vertices, MPI_INT, 0, MPI_COMM_WORLD);


    if ((rank != 0) && (rank != num_vertices - 1)){

        //local variables required for the other processes
        int loc_inqueue[num_vertices];
        int loc_outqueue[num_vertices];
        
        //int loc_capacities[num_vertices][num_vertices];
        int **loc_capacities, **loc_flow, *send_master, *source_nodes;

        //to send to the receiving vertex
        int pushed_amount;
        
        send_master    = (int *) calloc(num_vertices, sizeof(int));
        source_nodes   = (int *) calloc(num_vertices, sizeof(int));
        loc_capacities = (int **) calloc(num_vertices + 1, sizeof(int *));
        loc_flow       = (int **) calloc(num_vertices + 1, sizeof(int *));

        for (int i = 1; i <= num_vertices; ++i){
            loc_capacities[i] = (int *) calloc(num_vertices + 1, sizeof(int));
            loc_flow[i]       = (int *) calloc(num_vertices + 1, sizeof(int));
        }
        
        int rec_capacities[num_vertices * num_vertices];
        int rec_flow[num_vertices * num_vertices];
        
        int in_sum = 0, out_sum = 0, index = 0, i, j, surplus;
        int vertex_out, vertex_in;

        // variable to check whether a vertex is processed or not
        int processed = 0, recheck = 1;

        // receiving inqueue from master
        MPI_Recv(loc_inqueue, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        
        // receiving outqueue form master
        MPI_Recv(loc_outqueue, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        // receiving capacities copy form master
        MPI_Recv(rec_capacities, num_vertices * num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(rec_flow, num_vertices * num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (i = 1; i <= num_vertices; i++) {
            for (j = 1; j <= num_vertices; j++) {
                loc_capacities[i][j] = rec_capacities[index];
                loc_flow[i][j]       = rec_flow[index];
                index ++;
            }
        }

        // pushing the flow from the process where it is possible
        push_routine:
            in_sum = get_mat_column_sum(rank + 1, loc_flow);
            
            
            for (int i = 0; i < num_vertices; i++) {
                
                out_sum += loc_outqueue[i];
            }

            if (in_sum > 0 && processed == 0) {
                
                processed = 1;
                recheck = 0;
                surplus = in_sum - out_sum;
                for (i = 1; i <= num_vertices; i++) {
                    if(loc_capacities[rank + 1][i] != 0){
                        
                        // TESTING 
                        //printf("for vertex %d surplus is %d\n", rank + 1, surplus);

                        pushed_amount = push(loc_capacities, loc_flow, surplus, rank + 1, i);
                        
                        // TESTING
                        //printf("vertex %d sending to %d the flow %d\n", rank + 1, i, pushed_amount);
                        
                        // TESTING
                        //printf("Rank %d sending to %d the flow %d", rank, i, pushed_amount);
                        

                        MPI_Send(&pushed_amount, 1, MPI_INT, i - 1, 0, MPI_COMM_WORLD);
                        surplus -= pushed_amount;
                        
                        // TESTING
                        //printf("Surplus after push %d\n", surplus);
                    }
                }

                /* TESTING
                * printf("rank :%d, surplus: %d\n", rank, surplus);
                */

            }else{

                source_nodes = get_mat_column(rank + 1, loc_capacities, source_nodes);
                
                for (int i = 0; i < num_vertices; ++i){
                    if (source_nodes[i] != 0){
                    
                        MPI_Recv(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

                        /* TESTING
                        * printf("I am vertex %d and from '%d' i received : %d\n", rank + 1, i + 1, pushed_amount);
                        */

                        // updating the flow matrix
                        loc_flow[i + 1][rank + 1] += pushed_amount;
                        recheck = 1;
                    }
                }
            }

            if (recheck == 1)
            {
                goto push_routine;
            }
            
        /*TESTING
        *printf("For process %d\n", rank);
        *printf("local flows :\n");
        *print_mat(loc_flow);
        */

        // sending updated flows and surplus to the master
        send_master = loc_flow[rank + 1] + 1;
        MPI_Send(send_master, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD);
        printf("I am vertex %d and my surplsu is : %d \n", rank + 1, surplus);
        MPI_Send(&surplus, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
  
    }

    if (rank == 0)
    {
        int *updated_flow, *surplus, temp_surplus;

        updated_flow = (int *)malloc(sizeof(int) * num_vertices);
        surplus      = (int *)calloc(num_vertices + 1, sizeof(int));

        // receiving updates from childrens
        for (int i = 1; i < num_vertices - 1; ++i){
           MPI_Recv(updated_flow, num_vertices, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           MPI_Recv(&temp_surplus, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           printf("From %d the surplus received : %d\n", i, temp_surplus);
           surplus[i + 1] = temp_surplus;
           for (int j = 1; j <= num_vertices; ++j)
           {
               flow[i + 1][j] = updated_flow[j - 1];
               
                //printf("flow from %d to %d is :%d\n", j, i+1, flow[j][i+1]);
           }
           printf("\n");
        }

        

        // master copy of flow
        printf("Master copy of updated flows : \n");
        print_mat(flow);

        // final updation
        up_iteration(capacities, flow, surplus, 1, num_vertices);

         // Final flows
        printf("Master copy of updated flows : \n");
        print_mat(flow);


               
    }
    MPI_Finalize();

}
