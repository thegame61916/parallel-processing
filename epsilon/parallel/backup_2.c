#include "para_epsilon.h"

// Printing the matrix values
void print_mat(int **mat) {
    int i, j;
    
	printf("------------------------------\n");

    for (i = 0; i < num_vertices; i++) {
        for (j = 0; j < num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }

    printf("\n");
	printf("------------------------------\n\n");
}

// getting the column of a matrix
int *get_mat_column(int index, int **mat, int *temp){

    for (int i = 0; i < num_vertices; ++i){
        
        temp[i] = mat[i][index];
    }

    return temp;
}

//getting the column sum for particular index of the matrix
int get_mat_column_sum(int index, int **mat){

    int sum = 0;
    for (int i = 0; i < num_vertices; ++i){
        
        sum += mat[i][index];
    }

    return sum;
}

/*-----------SUPLEMENTARY METHODES FOR PARALLEL VERSION--------*/

int min_inflow(int vertex, int **c){

    int min = INFINITE, i;
    
    while(vertex != 0){

        for (i = 0; i < num_vertices; ++i){
            
            if (c[i][vertex] < min){
                min = c[i][vertex];
                vertex = i;
            }
        }
    }

    return 1;
    //return c[i][vertex];
}


/*-------------------------------------------------------------*/



/*--------------CODE OF ACTUAL SERIAL ALGO---------------------*/
//to provide initial preflow from source to connected vertices  
void preflow(int **c, int **f, int source){
    
    for (int i = 0; i < num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            //surplus[i]     = c[source][i];
            //surplus[source] -= c[source][i];
        }
    }
}

int push(int **c, int **f, int surplus, int u, int v){
    int push_flow = MIN(surplus, c[u][v] - f[u][v]);
    f[u][v]   += push_flow;

    return push_flow;
}

void relabel(int **c, int **f, int *height, int u){

    int min_height = INFINITE;
    for (int v = 0; v < num_vertices; v++) {
        if (c[u][v] - f[u][v] > 0) {
            min_height = MIN(min_height, height[v]);
            height[u] = min_height + 1;
        }
    }
}
/*-------------------------------------------------------------*/

int main(int argc, char const *argv[])
{
	clock_t tic = clock();

	MPI_Init(NULL, NULL);
    
    int size, rank;
    
    MPI_Request reqs[num_vertices];
    MPI_Status stats[num_vertices];
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   

    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    // Master

    if (rank == 0){
    	
		FILE *fp;
	    if (argv[1] == NULL) {
	        printf("Format is : ./push-relabel <graphfile>\n");
	        exit(0);
	    }else {
	        //output file
	        //fo = fopen("output.txt","w");
	        //input file
	        fp = fopen(argv[1], "r");   
	    }


	    //getting number of vertices and edges
	    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);

	    //initializing the flows and the capacities
	    flow       = (int **) calloc(num_vertices, sizeof(int*));
	    capacities = (int **) calloc(num_vertices, sizeof(int*));

	    for (i = 0; i < num_vertices; i++) {
	        flow[i]       = (int *) calloc(num_vertices, sizeof(int));
	        capacities[i] = (int *) calloc(num_vertices, sizeof(int));
	    }

	    //getting the info about capacities and edges
	    for (i = 0; i < num_edges; i++) {

	        fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
	        capacities[from][to] = capacity;
	    }
	    
	    //printing the capacities of the matrix
	    printf("\nCapacities:\n");
	    print_mat(capacities);

        preflow(capacities, flow, 0);

        printf("\nAfter preflow:\n");
        print_mat(flow);

        //serializing the capacities 2d array for the purpose of broadcasting
        int index = 0;
        int *updated_flow;

        updated_flow    = (int *)malloc(sizeof(int) * num_vertices);
        capacities_copy = (int *)malloc(sizeof(int) * num_vertices * num_vertices);
        flow_copy       = (int *)malloc(sizeof(int) * num_vertices * num_vertices);

        for (int i = 0; i < num_vertices; i++) {
            for (int j = 0; j < num_vertices; j++) {
                capacities_copy[index] = capacities[i][j];
                flow_copy[index]       = flow[i][j];
                index ++;
            }
        }

        //distributing job
        for (int i = 0; i < size - 2; ++i){
            
            int temp[num_vertices];
            inqueue  = get_mat_column(i + 1, flow, temp);
            outqueue = flow[i + 1];

            // sending inqueue to the respective processes
            MPI_Send(inqueue, num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

            // sending outqueue to the respective processes
            MPI_Send(outqueue, num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

            // sending capacities to all the processes
            MPI_Send(capacities_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
            MPI_Send(flow_copy, num_vertices * num_vertices, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

       }

     
    }

    // broadcasting number of vertices and edges to the other processes.
    MPI_Bcast(&num_vertices, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_edges, 1, MPI_INT, 0, MPI_COMM_WORLD);
    //MPI_Bcast(capacities_copy, num_vertices * num_vertices, MPI_INT, 0, MPI_COMM_WORLD);


    if ((rank != 0) && (rank != num_vertices - 1)){

        //local variables required for the other processes
        int loc_inqueue[num_vertices];
        int loc_outqueue[num_vertices];
        
        //int loc_capacities[num_vertices][num_vertices];
        int **loc_capacities, **loc_flow, *send_master, *source_nodes;

        //to send to the receiving vertex
        int pushed_amount;
        
        send_master    = (int *) calloc(num_vertices, sizeof(int));
        source_nodes   = (int *) calloc(num_vertices, sizeof(int));
        loc_capacities = (int **) calloc(num_vertices, sizeof(int *));
        loc_flow       = (int **) calloc(num_vertices, sizeof(int *));

        for (int i = 0; i < num_vertices; ++i){
            loc_capacities[i] = (int *) calloc(num_vertices, sizeof(int));
            loc_flow[i]       = (int *) calloc(num_vertices, sizeof(int));
        }
        
        int rec_capacities[num_vertices * num_vertices];
        int rec_flow[num_vertices * num_vertices];
        int in_sum = 0, out_sum = 0, index = 0, i, j, surplus;
        int vertex_out, vertex_in;

        // variable to check whether a vertex is processed or not
        int processed = 0, recheck = 1;

        // receiving inqueue from master
        MPI_Recv(loc_inqueue, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    	
        // receiving outqueue form master
        MPI_Recv(loc_outqueue, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        // receiving capacities copy form master
        MPI_Recv(rec_capacities, num_vertices * num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(rec_flow, num_vertices * num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (i = 0; i < num_vertices; i++) {
            for (j = 0; j < num_vertices; j++) {
                loc_capacities[i][j] = rec_capacities[index];
                loc_flow[i][j]       = rec_flow[index];
                index ++;
            }
        }

        // calulating flow reaching towards the vertex under processing 
        //in_sum = min_inflow(rank, loc_capacities);
        //printf("in sum for process %d is %d \n", rank, in_sum);

        // pushing the flow from the process where it is possible

            for (int i = 0; i < num_vertices; i++) {
                in_sum += loc_inqueue[i];
                out_sum += loc_outqueue[i];
            }

            if (in_sum > 0 && processed == 0) {
                processed = 1;
                recheck = 0;
                surplus = in_sum - out_sum;
                for (i = 0; i < num_vertices; i++) {
                    if(loc_capacities[rank][i] != 0){
                        printf("hello my rank is : %d and i am %d: ", rank, processed);
                        pushed_amount = push(loc_capacities, loc_flow, surplus, rank, i);
                        MPI_Send(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                        surplus -= pushed_amount;
                    }
                }
                //printf("rank :%d, surplus: %d\n", rank, surplus);
            }else{

                source_nodes = get_mat_column(rank, loc_capacities, source_nodes);
                for (int i = 0; i < num_vertices; ++i){
                    if (source_nodes[i] != 0){
                    
                        MPI_Recv(&pushed_amount, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                        printf("I am rank %d and from '%d' i received : %d\n", rank, i, pushed_amount);

                        //updating the flow matrix
                        loc_flow[i][rank] = pushed_amount;
                        recheck = 1;
                    }
                }
            }
            
        printf("For process %d\n", rank);
        printf("local flows :\n");
        print_mat(loc_flow);
        // sending updated flows to the master
        
        //send_master = loc_flow[rank];
        //MPI_Send(send_master, num_vertices, MPI_INT, 0, 0, MPI_COMM_WORLD);


        
        

        
       
        // predicting the list of vertices who might have done the flow update
        /*--
        for (int i = 0; i < num_vertices; ++i){
            vertex_in = get_mat_column_sum(i, loc_flow);
            if (vertex_in != 0){
                printf("Predicted vertex : %d\n", i);
                //MPI_Bcast(recb_flow, num_vertices, MPI_INT, i, MPI_COMM_WORLD);
            }
            
        }
        
        
        printf("Hi I am process %d and my surplus is %d : \n", rank, surplus);
        for (int j = 0; j < num_vertices; ++j){

            printf("%d\t", recb_flow[j]);
        }
        printf("\n");
        --*/

        /*--
        printf("local flows :\n");
        print_mat(loc_flow);
        --*/
        
        // calculating surplus at each node
        //for (int i = 0; i < num_vertices; ++i){
        //    in_sum  += loc_inqueue[i];
        //    out_sum += loc_outqueue[i];
        //}

        //surplus = in_sum > out_sum ? in_sum - out_sum : 0;

        //printf("Hi I am process %d and my surplus is %d : \n", rank, surplus);
        
    }

    /*--
    if (rank == 0)
    {
        int *updated_flow;

        updated_flow    = (int *)malloc(sizeof(int) * num_vertices);
        
        // receiving updates from childrens
        for (int i = 1; i < num_vertices - 1; ++i){
           MPI_Recv(updated_flow, num_vertices, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
           printf("From rank : %d master receiving :\n", i);
           for (int j = 0; j < num_vertices; ++j)
           {
               printf("%d\t", updated_flow[j]);
           }
           printf("\n");
        }
       
    }
    --*/
    MPI_Finalize();

}
