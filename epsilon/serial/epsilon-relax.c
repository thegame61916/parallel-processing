#include "epsilon-relax.h"
void print_mat(int **mat) {
    int i, j;
    
	printf("------------------------------\n");

    for (i = 1; i <= num_vertices; i++) {
        for (j = 1; j <= num_vertices; j++)
            if (mat[i][j] > 0) {
                printf("from %d to %d : %d\n", i, j, mat[i][j]);
            }
    }
	printf("\n------------------------------\n\n");
}

//test function for printing the admissilble edges of a vertex
void print_admissible(int **mat) {
    int i, j;
    
    printf("\n\nAdmissible edges are : \n");
	printf("------------------------------\n");

    j = 1;
    for (i = 2; i <= num_vertices; i++) {
        
        if (mat[i][j] == 0) {
            break;
        }else {
            printf("%d-->%d", i, mat[i][j]);
            j ++;
        }
    }
    printf("\n------------------------------\n\n");
}


//to provide initial preflow from source to connected vertices
void preflow(int **c, int **f, int *surplus, int source){
    
    for (int i = 1; i <= num_vertices; i++) {
        if (c[source][i] != 0) {
            f[source][i] += c[source][i];
            f[i][source] -= c[source][i];
            surplus[i]     = c[source][i];
            surplus[source] -= c[source][i];
        }
    }
}

//push operation is easy to understand
int push(int **c, int **f, int *surplus, int u, int v){
    int push_flow = MIN(surplus[u], c[u][v] - f[u][v]);
    f[u][v]   += push_flow;
    f[v][u]   -= push_flow;
    surplus[u] -= push_flow;
    surplus[v] += push_flow;

    return push_flow;
}

void incr_price(int u, int *price, int **neighbour, int **f, int **c){

        int min_height = INFINITE;
        for (int v = 1; v <= num_vertices; v++) {
            if (c[u][v] - f[u][v] > 0) {
                min_height = MIN(min_height, price[v]);
                price[u] = min_height + EPSILON;
            }
        }
}

//generating push-list containing the admissible  arcs
void generate_pushlist(int vertex, int *price, int **neighbour, int **admissible, int **f, int **c){
    
    int j, index, nvertex;
        
    j = 1;
    index = 1;
    while(neighbour[vertex][j] != 0){

        nvertex = neighbour[vertex][j];

        //e+ balanced arcs condition
        if ((price[vertex] == price[nvertex] + EPSILON) && (f[vertex][nvertex] < c[vertex][nvertex])) {
            admissible[vertex][index] = nvertex;
            index ++;
        }else if((c[vertex][nvertex] == 0) && (f[nvertex][vertex] > LB)) {
            admissible[vertex][index] = nvertex;
            index ++;
        }
        j ++;
    }

}

int up_iteration(int **c, int **f, int source, int sink){

    int i, j = 0, sum = 0, count = 0, push_index;
    int *surplus      = (int *)calloc(num_vertices + 1, sizeof(int));
    int *surplus1      = (int *)calloc(num_vertices + 1, sizeof(int));
    int *price        = (int *)calloc(num_vertices + 1, sizeof(int));
    int *price1        = (int *)calloc(num_vertices + 1, sizeof(int));
    int *intermediate = (int *)calloc(num_vertices - 1, sizeof(int));
    
    
    int **admissible   = (int **)calloc(num_vertices + 1, sizeof(int));
    int **neighbour    = (int **)calloc(num_vertices + 1, sizeof(int));

    for (i = 1; i <= num_vertices; i++) {
        admissible[i] = (int *)calloc(num_vertices + 1, sizeof(int));
        neighbour[i]  = (int *)calloc(num_vertices + 1, sizeof(int));
    }
	
	for (i = 0; i < num_vertices*2; i++) {
        for (j = 0; j < num_vertices*2; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    surplus[k] = 0;
                    price[k] = 0;
            }
        }           
    }

    
    //initializing the price of source
    price[source]   = 1;
    surplus[source] = INFINITE;

    //initializing with preflow
    preflow(c, f, surplus, source);

    printf("after the preflow : \n");
    print_mat(f);

    //getting intermediate nodes for processing
    for (i = 1; i <= num_vertices; i++) {
        if (i != 1 && i != num_vertices - 1) {
            intermediate[i - 1] = i;
        }

        //getting the neighbours
        count = 1;
        for (j = 1; j <= num_vertices; j++) {
            if (( c[i][j] != 0 ) || ( c[j][i] != 0)) {
                neighbour[i][count] = j;
                count ++;
            }
        }
    }

    //processing each node for push-list with no price alteration. 
    for (i = 2; i < num_vertices; i++) {
        for (j = 3; j <= num_vertices; j++) {
            if (c[i][j] != 0) {
                push(c, f, surplus, i, j);
            }
        }
    }
    
    for (i = 0; i < num_vertices*2; i++) {
        for (j = 0; j < num_vertices*2; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    surplus1[k] = 0;
                    price1[k] = 0;
            }
        }           
    }
    printf("after the first update : \n");
    print_mat(f);

    //generating the pushlist for incedent arcs and discharging respective surpluses
    for (i = 2; i < num_vertices; i++) {
        push_index = 1;
        if (surplus[i] > 0) {
            generate_pushlist(i, price, neighbour, admissible, f, c);
            if (surplus[i] > 0) {
                //print_admissible(admissible);
                push(c, f, surplus, i, admissible[i][push_index]);
                push_index ++;
                i = 1;
            }
        }
    }

    //performing operations considering single intermediate vertex at a time
    printf( "format : from-[sent flow]->to\n");
    for (i = 0; i < num_vertices*2; i++) {
        for (j = 0; j < num_vertices*2; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    surplus1[k] = 0;
                    price1[k] = 0;
            }
        }           
    }
    for (i = 0; i < num_vertices - 2; i++) {
        int u = intermediate[i];
        
    }

    //final labels on the vertices
    //printf( "\nfinal prices: \n");
    //printf( "------------------------------\n");
    
    //for (i = 1; i <= num_vertices; i++) {
    //    printf( "price[%d] : %d\n", i, price[i]);
    //    printf( "surplus[%d] : %d\n", i, surplus[i]);
    //}

    printf( "------------------------------\n");
    //maximum flow obtained at the sink
    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    surplus1[k] = i;
                    price1[k] = j;
            }
        }           
    }
    for (i = 1; i <= num_vertices; i++) {
        if (f[i][num_vertices] > 0) {
            sum += f[i][num_vertices];
        }
    }
    for (i = 0; i < num_vertices*10; i++) {
        for (j = 0; j < num_vertices*10; j++) {
            for (int k = 0; k < num_vertices; k++) {
                    surplus1[k] = i;
                    price1[k] = k;
            }
        }           
    }
    printf( "maximum flow : %d\n", sum);
    
}

int main(int argc, const char *argv[]) { 	

    clock_t tic = clock();   

    int **flow, **capacities, i, from, to, linenumber = 1, capacity;

    FILE *fp;
    if (argv[1] == NULL) {
        printf("format is : ./epsilon-relax <graphfile>\n");
        exit(0);
    }else {
        //output file
        fo = fopen("output.txt","w");
        //input file
        fp = fopen(argv[1], "r");   
    }

    //getting number of vertices and edges
    fscanf(fp, "%d\t%d", &num_vertices, &num_edges);
    
    //initializing the flows and the capacities
    flow       = (int **) calloc(num_vertices + 1, sizeof(int*));
    capacities = (int **) calloc(num_vertices + 1, sizeof(int*));

    for (i = 1; i <= num_vertices; i++) {
        flow[i]       = (int *) calloc(num_vertices + 1, sizeof(int));
        capacities[i] = (int *) calloc(num_vertices + 1, sizeof(int));
    }

    //getting the info about capacities and edges
    for (i = 1; i <= num_edges; i++) {

        fscanf(fp, "%d\t%d\t%d", &from, &to, &capacity);
        capacities[from][to] = capacity;
    }
    
    //printing the capacities of the matrix
    printf( "\ncapacities:\n");
    print_mat(capacities);

    up_iteration(capacities, flow, 1, num_vertices);
    

    //printing the final flow in the network
    printf( "\nflows:\n");
    print_mat(flow);

    clock_t toc = clock();
    printf( "execution time : %f seconds\n", (double)(toc - tic) / 1000000.0);
	printf("------------------------------\n");
    //system("cat output.txt");
    return 0;
}
